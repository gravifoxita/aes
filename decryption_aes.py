# Create by gravifoxita #

import struct
from Crypto.Cipher import AES

def unpad(data):
    return data[:-ord(data[len(data)-1:])]

def decrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    if not out_filename:
        out_filename = in_filename + ".decrypted"
    
    with open(in_filename, 'rb') as infile:
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        iv = infile.read(16)
        decryptor = AES.new(key, AES.MODE_CBC, iv)

        with open(out_filename, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(unpad(chunk)))

            outfile.truncate(origsize)

key = b'Sixteen byte key'
decrypt_file(key, "test.txt")
