# Create by gravifoxita #

import base64, hashlib
from Crypto.Cipher import AES

def pad(data):
    length = 16 - (len(data)%16)
    return data + bytes([length])*length

def encrypt(plaintext, password):
    password = hashlib.sha256(password.encode()).digest()
    cipher = AES.new(password, AES.MODE_ECB)
    ciphertext = cipher.encrypt(pad(plaintext))
    return base64.b64encode(ciphertext).decode()

def encrypt_file(file_name, password):
    with open(file_name, 'rb') as f:
        plaintext = f.read()
    encrypted = encrypt(plaintext, password)
    with open(file_name, 'wb') as f:
        f.write(encrypted.encode())

def secure_file(file_name):
    password = input("Entrer password: ")
    encrypt_file(file_name, password)
    print(f"File '{file_name}' est sécurisé")

if __name__ == "__main__":
    secure_file("test.txt")